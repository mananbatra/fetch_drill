
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/

const fetch=require('node-fetch');

const userApiUrl=`https://jsonplaceholder.typicode.com/users`;
const todoApiUrl= `https://jsonplaceholder.typicode.com/todos`;

//Solution 1
function fetchUsers() {
   return fetch(userApiUrl)
      .then(response => response.json())
      .then(users => {
        console.log(users);
        return users;
      })
      .catch(error => {
        console.log('Error fetching users:', error);
        throw error;
      });
}

//fetchUsers();

//Solution 2

function fetchTodos() {
    return fetch(todoApiUrl)
      .then(response => response.json())
      .then(todos => {
        console.log('Todos:', todos);
        return todos;
      })
      .catch(error => {
        console.log('Error fetching todos:', error);
        throw error;
      });
  }

//fetchTodos();

//Solution 3

function fetchUsersAndTodos() {
    return fetchUsers()
      .then(() => fetchTodos())
      .catch(error => {
        console.log('Error:', error);
        throw error;
      });
 }

fetchUsersAndTodos();
